function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
  
    //Conditons:
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        // If letter is invalid, return undefined.
    if (letter.length === 1) {
        return (letter.split(sentence).length-1)
    } else {
        return undefined
    }
    
}


function isIsogram(text) {
    //Goal:
        // An isogram is a word where there are no repeating letters.

    //Check:
        // The function should disregard text casing before doing anything else.


    //Condition:
        // If the function finds a repeating letter, return false. Otherwise, return true.
    for (let i = 0; i < text.length; i++) {
        if (text.indexOf(text[i] !== text.lastIndexOf(text[i]))){
            return false
        } else {
            return true
        }
    }
    
}

function purchase(age, price) {

    //Conditions:
        // Return undefined for people aged below 13.
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        // Return the rounded off price for people aged 22 to 64.

    if (age < 13) {
        return undefined
    } if (age >= 12 && age < 21 ) {
        return discountedPrice
    } if (age > 65) {
        return discountedPrice
    } if (age > 21 && age < 65) {
        return roundedPrice
    }


    //Check:
        // The returned value should be a string.
    
}

function findHotCategories(items) {
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Array for the test:
        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }


    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].

    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    for (let i = 0; i < items.length; i++) {
        if (items.stocks == 0) {
            return items.category
        } else {
            return `All items have stocks`
        }
    }


}

function findFlyingVoters(candidateA, candidateB) {
    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    //Array for the test:    
        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};